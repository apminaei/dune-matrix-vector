﻿#ifndef DUNE_MATRIX_VECTOR_ASFIELDMATRIX_HH
#define DUNE_MATRIX_VECTOR_ASFIELDMATRIX_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/matrix-vector/types/multitypematrix.hh>

namespace Dune {
namespace MatrixVector {

template <class>
struct AsFieldMatrix;

template <class M>
using AsFieldMatrix_t = typename AsFieldMatrix<M>::type;

template <class K, int n, int m>
struct AsFieldMatrix<FieldMatrix<K, n, m>> {
  using type = FieldMatrix<K, n, m>;
};

template <class K, int n>
struct AsFieldMatrix<DiagonalMatrix<K, n>> {
  using type = FieldMatrix<K, n, n>;
};

template <class K, int n>
struct AsFieldMatrix<ScaledIdentityMatrix<K, n>> {
  using type = FieldMatrix<K, n, n>;
};

template <class BlockMatrix>
struct AsFieldMatrix<BCRSMatrix<BlockMatrix>> {
  using type = BCRSMatrix<AsFieldMatrix_t<BlockMatrix>>;
};

template <class... Args>
struct AsFieldMatrix<MultiTypeBlockMatrix<Args...>> {
  using type = MultiTypeBlockMatrix<AsFieldMatrix_t<Args>...>;
};

template <class... Args>
struct AsFieldMatrix<MultiTypeMatrix<Args...>> {
  using type = MultiTypeMatrix<AsFieldMatrix_t<Args>...>;
};

} // end namespace MatrixVector
} // end namespace Dune

#endif // DUNE_MATRIX_VECTOR_ASFIELDMATRIX_HH
